﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LibraryStore.DAO;
using LibraryStore.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace LibraryStore.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BooksController : ControllerBase
    {
        private BooksDAO booksDao = new BooksDAO();

        // GET: api/Books
        [HttpGet]
        public List<Books> GetBooks()
        {
            return booksDao.GetBooks();
        }

        // GET: api/Books
        [HttpGet("{id}")]
        public Books GetBookById(int id)
        {
            return booksDao.GetBookById(id);
        }

        // POST: api/Books
        [HttpPost]
        public void InsertBook(Books book)
        {
            booksDao.InsertBooks(book);
        }

        // PUT: api/Books/{id}
        [HttpPut("{id}")]
        public void UpdateBook(int id, Books book)
        {
            booksDao.UpdateBook(id, book);
        }

        // DELETE: api/Books/{id}
        [HttpDelete("{id}")]
        public void DeleteBook(int id)
        {
            booksDao.DeleteBook(id);
        }
    }
}