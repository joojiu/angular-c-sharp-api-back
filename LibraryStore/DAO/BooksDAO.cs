﻿using LibraryStore.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace LibraryStore.DAO
{
    public class BooksDAO
    {
        private SqlConnection connection = new SqlConnection(@"Persist Security Info=False;User ID=jooji;Password=123456;Initial Catalog=jooji;Data Source=PLTW10N07412\SQLEXPRESS");
        public List<Books> GetBooks()
        {
            string commandText = "SELECT * FROM dbo.Books";
            SqlCommand cmd = new SqlCommand(commandText, connection);
            List<Books> books = new List<Books>();
            try
            {
                connection.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    Books book = new Books();
                    book.Id = Convert.ToByte(reader["id"]);
                    book.Name = Convert.ToString(reader["name"]);
                    book.Type = Convert.ToString(reader["typeBook"]);
                    book.Price = Convert.ToInt32(reader["price"]);
                    book.Quantity = Convert.ToInt32(reader["quantity"]);

                    books.Add(book);
                }

                return books;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close();
            }
        }

        public Books GetBookById(int id)
        {
            string commandText = "SELECT * FROM dbo.Books WHERE id = " + id;
            SqlCommand cmd = new SqlCommand(commandText, connection);
            Books book = new Books();

            try
            {
                connection.Open();
                SqlDataReader reader = cmd.ExecuteReader();

                if(reader.Read())
                {
                    book.Id = Convert.ToByte(reader["id"]);
                    book.Name = Convert.ToString(reader["name"]);
                    book.Type = Convert.ToString(reader["typeBook"]);
                    book.Price = Convert.ToInt32(reader["price"]);
                    book.Quantity = Convert.ToInt32(reader["quantity"]);

                    return book;
                }
                else
                {
                    throw new System.ArgumentException("Parameter cannot be null", "original");
                }
            }
            catch(Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close();
            }
        }

        public void InsertBooks(Books book)
        {
            string commandText = "INSERT INTO dbo.Books(name, typeBook, price, quantity) VALUES (@name, @type, @price, @quantity)";
            SqlCommand cmd = new SqlCommand(commandText, connection);

            cmd.Parameters.AddWithValue("@name", book.Name);
            cmd.Parameters.AddWithValue("@type", book.Type);
            cmd.Parameters.AddWithValue("@price", book.Price);
            cmd.Parameters.AddWithValue("@quantity", book.Quantity);

            try
            {
                connection.Open();
                cmd.ExecuteNonQuery();
            }
            catch(Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close();
            }
        }

        public void UpdateBook(int id, Books book)
        {
            string commandText = "UPDATE Books SET name = @name, typeBook = @type, price = @price, quantity = @quantity WHERE id = " + id;
            SqlCommand cmd = new SqlCommand(commandText, connection);

            cmd.Parameters.AddWithValue("@name", book.Name);
            cmd.Parameters.AddWithValue("@type", book.Type);
            cmd.Parameters.AddWithValue("@price", book.Price);
            cmd.Parameters.AddWithValue("@quantity", book.Quantity);

            try
            {
                connection.Open();
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close();
            }
        }

        public void DeleteBook(int id)
        {
            string commandText = "DELETE FROM Books WHERE id = " + id;
            SqlCommand cmd = new SqlCommand(commandText, connection);

            try
            {
                connection.Open();
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close();
            }
        }
    }
}
